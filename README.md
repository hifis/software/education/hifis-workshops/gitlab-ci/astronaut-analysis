<!--
SPDX-FileCopyrightText: 2022 Helmholtz Centre for Environmental Research (UFZ)
SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)

SPDX-License-Identifier: CC0-1.0
-->

# Astronaut Analysis Example

## Giving Credit

This example project is taken from the following project:
[Astronaut Analysis](https://codebase.helmholtz.cloud/hifis/software/education/hifis-workshops/foundations-of-research-software-publication/astronaut-analysis).

## Purpose of this Repository

This project will be used to teach workshop participants by example how
GitLab CI works.
Each step is isolated in different Git branches as checkpoints participants
can follow along after each episode or exercise.

## Project Outline and Data

In this project astronaut data is taken from 
[Wikidata](https://www.wikidata.org/wiki/Wikidata:Main_Page)
to analyse the time humans spent in space as well as the age distribution
of the astronauts.
The SPARQL query to generate the data can be inspected in the Wikidata Query
Service: 
[SPARQL Query](https://query.wikidata.org/#%23Birthplaces%20of%20astronauts%0ASELECT%20DISTINCT%20%3Fastronaut%20%3FastronautLabel%20%3Fbirthdate%20%3FbirthplaceLabel%20%3Fsex_or_genderLabel%20%3Ftime_in_space%20%3Fdate_of_death%20WHERE%20%7B%0A%20%20%3Fastronaut%20%3Fx1%20wd%3AQ11631.%0A%20%20%3Fastronaut%20wdt%3AP569%20%3Fbirthdate.%0A%20%20%3Fastronaut%20wdt%3AP19%20%3Fbirthplace.%0A%20%20SERVICE%20wikibase%3Alabel%20%7B%20bd%3AserviceParam%20wikibase%3Alanguage%20%22en%22.%20%7D%0A%20%20OPTIONAL%20%7B%20%3Fastronaut%20wdt%3AP21%20%3Fsex_or_gender.%20%7D%0A%20%20OPTIONAL%20%7B%20%3Fastronaut%20wdt%3AP2873%20%3Ftime_in_space.%20%7D%0A%20%20OPTIONAL%20%7B%20%3Fastronaut%20wdt%3AP570%20%3Fdate_of_death.%20%7D%0A%7D%0AORDER%20BY%20DESC%28%3Ftime_in_space%29).

## Plots Created During Astronaut Analysis

![Total Time Human in Space](results/humans_in_space.png)
![Total Time Females in Space](results/females_in_space.png)
![Total Time Males in Space](results/males_in_space.png)
![Age Distribution Box Plot](results/age_box_plot.png)
![Age Distribution Histogram](results/age_histogram.png)

## Aspects of Continuous Integration Covered in Branches

| Branch Name               | Purpose of the Branch                                               |
|---------------------------|---------------------------------------------------------------------|
| main                      | Starting point of the project without CI.                           |
| 3-basic-ci-pipeline       | Basic CI pipeline has been added.                                   |
| 4-extending-the-pipeline  | CI pipeline has been extended with common use cases.                |
| 5-removing-redundancies   | Redundancies have been removed from CI pipeline.                    |
| 6-optimizing-the-pipeline | CI pipeline has been optimized to let run more CI jobs in parallel. |
| 8-using-includes          | Structure of CI pipeline has been adapted to use multiple files.    |

## Set Up Project

### Clone Project

The _Git_ project can be cloned like so:

```bash
git clone https://codebase.helmholtz.cloud/hifis/software/education/hifis-workshops/gitlab-ci/astronaut-analysis.git
```

### Install uv

The package manager _uv_ helps you with installing the project dependencies.
Please read the
[installation guide](https://docs.astral.sh/uv/getting-started/installation/#installing-uv)
to install _uv_ on different _OS_.

### Install Dependencies

You can use Make to install the project dependencies with Poetry:

```bash
uv venv
source .venv/bin/activate
uv sync
```

### Run Project

You can run the analysis via this command:

```bash
uv run python -m astronaut_analysis
```

### Test Project

Execute the test suite with this command:

```bash
uv run pytest tests/ 
```

### Lint Project

Linting of the project is done with the following commands:

```bash
uv run isort --check --diff src/ tests/
uv run black --check --diff src/ tests/
uv run reuse lint
```

## Contributors

The main contributors of this workshop are:

- Norman Ziegner
- Tobias Huste
- Christian Hueser

## License

Please read the file [LICENSE.md](LICENSE.md) to get information on the
licenses used.
